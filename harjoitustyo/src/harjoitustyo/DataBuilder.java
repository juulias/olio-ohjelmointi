/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyo;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author juulia
 */
public class DataBuilder {
    private ArrayList<SmartPost> all; // list of all the SmartPost objects
    private ArrayList<SmartPost> selected; // list of all the selected SmartPost objects
    
    public DataBuilder(String url) throws IOException, ParserConfigurationException, SAXException {
        // GetXML object handles the XML data
        GetXML xml = new GetXML(url);

        all = new ArrayList();
        selected = new ArrayList();
        
        // Load all the possible SmartPosts to the ArrayList
        all = xml.getAllSmartPost();
    }
    
    // Create new SmartPost from users selection to the selected-arraylist.
    // Remove it from the list of possible choices, as we don't want the
    // user to choose the same location for multiple times
    public SmartPost addSmartPost(SmartPost a) {
        SmartPost SP = a;        
        selected.add(SP);
        all.remove(SP);
        return SP;
    }
    
    // Return all the SmartPost objects
    public ArrayList<SmartPost> getSmartPosts () {
        return all;
    }
    
    // Return all the user's selected SmartPost objects
    public ArrayList<SmartPost> getSelectedSmartPosts () {
        return selected;
    }
}



class GetXML {
    private Document doc;
    private ArrayList<SmartPost> all = new ArrayList();

    public GetXML(String url) throws IOException, ParserConfigurationException, SAXException {
        
        try {
            // Get the XML from internet
            String content = listContent(url);

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(content)));
            doc.getDocumentElement().normalize();
            
            // parse the data and create a SmartPost object
            parseCurrentData();
            
        } catch (ParserConfigurationException | SAXException | IOException ex) {
            Logger.getLogger(GetXML.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    // Get the XML from internet
    private String listContent(String x) throws MalformedURLException, IOException {
        URL url = new URL (x);
        BufferedReader br = new BufferedReader(new InputStreamReader(url.openStream()));
        String xmlContent = "";
        String line;
        while ((line = br.readLine()) != null) {
            xmlContent += line + "\n";
        }  
        return xmlContent;
    }
    
    // Parse XML data and create a new SmartPost location from it
    private void parseCurrentData() {
        NodeList nodes = doc.getElementsByTagName("place");
        for (int i = 0; i<nodes.getLength(); i++) {
            Node node = nodes.item(i);
            Element e = (Element) node;

            String city = e.getElementsByTagName("city").item(0).getTextContent();
            String address = e.getElementsByTagName("address").item(0).getTextContent();
            String code = e.getElementsByTagName("code").item(0).getTextContent();
            address = address + ", " + code + ", " + city;

            String opening = e.getElementsByTagName("availability").item(0).getTextContent();

            float lat = Float.parseFloat(e.getElementsByTagName("lat").item(0).getTextContent());
            float lon = Float.parseFloat(e.getElementsByTagName("lng").item(0).getTextContent());

            all.add(new SmartPost(city, address, opening, lat, lon));
        }

    }
    
    // Return a list of all the SmartPost objects
    public ArrayList<SmartPost> getAllSmartPost() {
        return all;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyo;

/**
 *
 * @author juulia
 */
public class SmartPost {
    
    private String city;
    private String address;
    private String opening;
    private GeoPoint geo;
    
    public SmartPost(String C, String A, String O, float x, float y) {
        city = C;
        address = A;
        opening = O;
        geo = new GeoPoint(x, y);
    }
    
    @Override
    public String toString() {
        return address;
    }
    
    // Methods to return objects attributes
    public String getCity() { return city;}
    public String getAddress() { return address;}
    public String getOpening() { return opening;}
    public float getX() { return geo.getLat();}
    public float getY() { return geo.getLon();}

}


// Class for the SmartPost's location
class GeoPoint {
    private float lat;
    private float lon;
    
    public GeoPoint(float x, float y) {
        lat = x;
        lon = y;
    }

    public float getLat() { return lat;}
    public float getLon() { return lon;}
}
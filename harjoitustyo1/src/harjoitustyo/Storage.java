/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyo;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author juulia
 */
public class Storage {
    ArrayList<Package> packages;
    
    public Storage () {
        packages  = new ArrayList();
    }
    
    // Add a package from the existing alternatives
    public String addPackage(String nameP, int classP) {
        Objects O;
        
        if (nameP.equals("piano")) {O = new piano();}
        else if (nameP.equals("tiiliskivi, 5kpl")) {O = new brick(); }
        else if (nameP.equals("untuvatakki")) {O = new jacket();}
        else {O = new plate();}
                
        if (classP == 0) {
            return "Muista valita haluamasi pakettiluokka!";
        // Use checkSize method to check, that the item fits
        // into the package
        } else if (checkSize(O, classP).length()!=0) {
            return checkSize(O, classP);
        }
        
        createPackage(O, classP);        
        return "Paketin tallentaminen onnistui!";
    }
    
    // Add the users own package
    public String addPackage(String nameP, String sizeP, String weightP, boolean canBreakP, int classP) {
        int width, height, depth, weight;
        if (nameP == null || sizeP == null || weightP == null) {
            return "Kaikki tekstikentät on täytettävä! Voit myös valita valmiin esineen pudotusvalikosta.";            
        }
        
        weight = Integer.parseInt(weightP);

        //Parsing the size attributes from users input
        String[] size = sizeP.split("\\*");
        if(size.length == 3) {
            width = Integer.parseInt(size[0]);
            height = Integer.parseInt(size[1]);
            depth = Integer.parseInt(size[2]);
        } else {
            return "Virhe koon syöttämisessä. Koko syötettävä muodossa cm*cm*cm!";
        }
        
        // Create Objects object from these information, and the check that
        // it fits into the chosen package
        Objects O = new Objects(nameP, width, height, depth, weight, canBreakP);
        
        if (classP == 0) {
            return "Muista valita haluamasi pakettiluokka!";
        } else if (checkSize(O, classP).length()!=0) {
            return checkSize(O, classP);
        }
        
        createPackage(O, classP);
        return "Paketin tallentaminen onnistui!";
    }
    

    private void createPackage(Objects O, int classP) {
        switch(classP) {
            case 1: packages.add(new FirstClass(O)); break;
            case 2: packages.add(new SecondClass(O)); break;
            case 3: packages.add(new ThirdClass(O)); break;
        }
    }
    
    // method for checking, that the item fits into the package
    private String checkSize(Objects O, int classP) {
        if ((classP == 1 && (O.getWidth() > 50 || O.getHeight() > 60 || O.getDepth() > 50 || O.getWeight() > 10)) ||
                (classP == 2 && (O.getWidth() > 30 || O.getHeight() > 30 || O.getDepth() > 30 || O.getWeight() > 2)) ||
                (classP == 3 && (O.getWidth() > 100 || O.getHeight() > 80 || O.getDepth() > 80 || O.getWeight() > 35))) {
            return "Esine on liian iso valitsemaasi pakettiluokkaan!\n"
                    + "Rajat:\n1. luokka: 50*60*50, paino 10kg\n"
                    + "2. luokka: 30*30*30, paino 2kg\n"
                    + "3. luokka: 100*80*80, paino 35kg";
        }
        return "";
    }
    
    
    // Method that returns the package, which includes a specific
    // item, based on the items name
    public Package getPackage(String s) {
        Package P = null;
        for (Package p : packages) {
            if (p.getName().equals(s)) {
                P = p;
            }
        }
        return P;
    }
    
    // returns all the packages
    public ArrayList getAll() {
        return packages;
    }
    
    // returns all the packages' item's name
    public ArrayList getAllString() {
        ArrayList<String> names = new ArrayList();
        for (Package p : packages)
            names.add(p.getName());
        return names;
    }
    
    public void clear() {
        packages.clear();
    }
    
    // remove a specific package with the name of its item
    public void clear(String s) {
        packages.remove(getPackage(s));
    }
    
    // Save the current storage into external .txt file
    public void saveStorage() {
        String filename = "storage.txt";
        
        try {
            BufferedWriter out;
            out = new BufferedWriter(new FileWriter(filename));
            for (Package p : packages) {
                out.write(p.getClassN() + ";");
                out.write(p.getName() + ";");
                out.write(p.getWidth() + ";");
                out.write(p.getHeight() + ";");
                out.write(p.getDepth() + ";");
                out.write(p.getWeight() + ";");
                out.write(p.canBreak() + "\n");
            }
            out.close();

        } catch (IOException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    // Load the previous storage from the external .txt file
    public void loadStorage() {
        String filename = "storage.txt";
        
        try {
            BufferedReader in;
            in = new BufferedReader(new FileReader(filename));
            String inputLine;
            while ((inputLine = in.readLine()) != null) {
                
                // Parse the content in order to get the needed information for
                // forming the objects
                
                String[] info = inputLine.split(";");
                int c = Integer.parseInt(info[0]);
                String n = info[1];
                int w = Integer.parseInt(info[2]);
                int h = Integer.parseInt(info[3]);
                int d = Integer.parseInt(info[4]);
                int we = Integer.parseInt(info[5]);
                boolean b = Boolean.parseBoolean(info[6]);
                
                createPackage(new Objects(n, w, h, d, w, b), c);
            }
            in.close();

        } catch (IOException ex) {
            Logger.getLogger(Storage.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package harjoitustyo;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Tab;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.scene.text.Font;
import javafx.scene.web.WebView;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;

/**
 *
 * @author juulia & ville
 */
public class FXMLDocumentController implements Initializable {

    // Introducing the objects on which the functionality is built on
    DataBuilder DB;
    Storage ST;
    Sending S;
    History H;
    
    
    // UI Components
    private Label label;
    @FXML
    private WebView kartta;
    @FXML
    private AnchorPane index1;
    @FXML
    private AnchorPane index2;
    @FXML
    private AnchorPane index3;
    @FXML
    private AnchorPane index4;
    @FXML
    private ComboBox<SmartPost> comboLocation;
    @FXML
    private Font x1;
    @FXML
    private Button addSP;
    @FXML
    private ComboBox<String> comboCity;
    @FXML
    private Font x2;
    @FXML
    private Button savePackage;
    @FXML
    private TextField packageName;
    @FXML
    private TextField packageSize;
    @FXML
    private TextField packageWeight;
    @FXML
    private ComboBox<String> listOfPackages;
    @FXML
    private CheckBox fragileCheck;
    @FXML
    private RadioButton class1;
    @FXML
    private RadioButton class2;
    @FXML
    private RadioButton class3;
    @FXML
    private Label messageCreate;
    @FXML
    private ListView<String> packageList;
    @FXML
    private ComboBox<SmartPost> startBox;
    @FXML
    private ComboBox<SmartPost> endBox;
    @FXML
    private Button sendPackage;
    @FXML
    private Label sendInformation;
    @FXML
    private Tab tabSend;
    @FXML
    private Button empty;
    @FXML
    private ListView<?> historyView;
    
/*******************************************************/

    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        kartta.getEngine().load(getClass().getResource("index.html").toExternalForm());
        
        try {
            ST = new Storage();
            H = new History();
            S = new Sending(H);
            
            // Handling the XML data with DataBuilder DB
            String urlXML = "http://smartpost.ee/fi_apt.xml";
            DB = new DataBuilder(urlXML);
            
            // Listing the city names in the first ComboBox
            ArrayList<String> cities = new ArrayList();
            for (SmartPost item : DB.getSmartPosts()) {
                if (!(cities.contains(item.getCity()))) {
                    comboCity.getItems().add(item.getCity());
                    cities.add(item.getCity());
                }
            }            
        } catch (SAXException | IOException | ParserConfigurationException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    
    //SelectCity will add the needed locations to the second ComboBox
    //based on what is selected for the city
    @FXML
    private void selectCity(ActionEvent event) {
        comboLocation.getItems().clear();
        for (SmartPost item : DB.getSmartPosts()) {
            if (item.getCity().equals(comboCity.getValue())) {
                comboLocation.getItems().add(item);

            }
        }
    }
    
    //Add SmartBox to the map
    @FXML
    private void addSP(ActionEvent event) {
        if (comboLocation.getValue() != null) {
            SmartPost sp = DB.addSmartPost(comboLocation.getValue());
            kartta.getEngine().executeScript("document.goToLocation('" + sp.getAddress() + "', '" + sp.getOpening() +"', 'blue')");
            
            // Update the combobox for choosing the starting point for routes
            startBox.getItems().add(sp);
        }
    }

    
    // Select starting location: after this, the method lists on the destination
    // combobox all the possible destinations except the one that was chosen for
    // starting point.
    @FXML
    private void selectStartPoint(ActionEvent event) {
        endBox.getItems().clear();
        for (SmartPost item : DB.getSelectedSmartPosts()) {
            if (!(item.getAddress().equals(startBox.getValue().getAddress()))) {
                endBox.getItems().add(item);
            }
        }
    }

    // Save package so that it can be sent.
    // The user's selections are checked, and an error message
    // is printed, if something is wrong.
    @FXML
    private void savePackage(ActionEvent event) {
        String message = "";
        int classP;
        
        if (class1.isSelected()) {
            classP = 1;
        } else if (class2.isSelected()) {
            classP = 2;
        } else if (class3.isSelected()) {
            classP = 3;
        } else {
            classP = 0;
        }
        
        // Check first if the user has chosen a ready made item
        if ((listOfPackages.getValue() != null) && !(listOfPackages.getValue().equals("oma esine"))) {
            message = ST.addPackage(listOfPackages.getValue(), classP);
            
        }
        // Otherwise, the information in the labels is handled
        else {
            if (packageName.getText().isEmpty() || packageSize.getText().isEmpty() || packageWeight.getText().isEmpty()) {
                message ="Kaikki tekstikentät on täytettävä! Voit myös valita valmiin esineen pudotusvalikosta.";
            } else {
                String name = packageName.getText();
                String size = packageSize.getText();
                String weight = packageWeight.getText();
                boolean canBreak;
                
                if (fragileCheck.isSelected()) {
                    canBreak = true;
                } else {
                    canBreak = false;
                }
                
                message = ST.addPackage(name, size, weight, canBreak, classP);
            }
        }
        
        messageCreate.setText(message);
        
        //finally items are loaded also into the list for sending
        loadPackages();
    }
    
    // Method for loading the list of existing packages for sending
    private void loadPackages() {
        ArrayList<String> packagesToSend = ST.getAllString();
        ObservableList list = FXCollections.observableArrayList(packagesToSend);
        packageList.setItems(list);
    }

    // Method for clearing the list of existing packages
    @FXML
    private void clearPackageList(ActionEvent event) {
        ArrayList<String> empty = new ArrayList();
        ObservableList list = FXCollections.observableArrayList(empty);
        packageList.setItems(list);
        ST.clear();
    }
    
    // Method for sending the already created packages
    @FXML
    private void sendPackage(ActionEvent event) throws IOException {
        
        // Checking that the user has given all the necessary information
        if (packageList.getSelectionModel().getSelectedItem() == null) {
            sendInformation.setText("Valitse lähetettävä paketti! Jos lista on tyhjä, sinun tarvitsee lisätä paketti toisessa välilehdessä.");
        } else if (startBox.getValue() == null || endBox.getValue() == null) {
            sendInformation.setText("Valitse sekä lähtö- että kohdeautomaatti. Jos lista on tyhjä, sinun tarvitsee lisätä SmartPost automaatteja toisessa välilehdessä.");
        } else {
            
            // Sending the package with S.send -method
            String s = packageList.getSelectionModel().getSelectedItem();
            String javascript = S.send(startBox.getValue(), endBox.getValue(), ST.getPackage(s));
            sendInformation.setText("Postitetaan:\n" + s + ", luokka " + ST.getPackage(s).getClassN() + "\n");

            if (!(ST.getPackage(s).objectIntact())) {
                sendInformation.setText(sendInformation.getText() + "Esine hajosi matkalla!");
            }
            
            // clearing the package from the list
            ST.clear(s);
            loadPackages();
            
            //Javascript script for visualization
            kartta.getEngine().executeScript(javascript);
            
            //Current history view updated
            ObservableList list = FXCollections.observableArrayList(H.listHistory(S.getHistory()));
            historyView.setItems(list);
        }
    }

    // Clear the drawn routes
    @FXML
    private void emptyRoutes(ActionEvent event) {
        kartta.getEngine().executeScript("document.deletePaths()");
        
    }

    // Save a log of this time's actions.
    // The log is saved as logi.txt
    // Overwrites previous log.
    @FXML
    private void saveLog(ActionEvent event) {
        try {
            S.saveLog(ST.getAll());
        } catch (IOException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    // Save the current storage for later use.
    // OverWrites previous storage.txt.
    @FXML
    private void saveStorage(ActionEvent event) {
        ST.saveStorage();
    }

    // Load saved storage: includes the items that were previouslu
    // created but not yet sent.
    @FXML
    private void loadStorage(ActionEvent event) {
        ST.loadStorage();
        loadPackages();
    }

    // Load the complete history to the history page.
    @FXML
    private void loadCompleteH(ActionEvent event) {
        
        ObservableList list = FXCollections.observableArrayList(H.listHistory(H.loadCompleteLog()));
        historyView.setItems(list);
    }


    
}
